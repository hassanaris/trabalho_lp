  <!-- photography_slider_area_start -->
  <div class="photography_slider_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="photoslider_active owl-carousel">
                        <div class="single_photography">
                            <img src="<?php echo base_url('assets/img/photography/single-1.jpg')?>" alt="">
                            <div class="photo_title">
                                <h4>Photography</h4>
                            </div>
                        </div>
                        <div class="single_photography">
                            <img src="<?php echo base_url('assets/img/photography/single-2.jpg')?>" alt="">
                            <div class="photo_title">
                                <h4>Travel Shot</h4>
                            </div>
                        </div>
                        <div class="single_photography">
                            <img src="<?php echo base_url('assets/img/photography/single-3.jpg')?>" alt="">
                            <div class="photo_title">
                                <h4>Photoshop</h4>
                            </div>
                        </div>
                        <div class="single_photography">
                            <img src="<?php echo base_url('assets/img/photography/single-4.jpg')?>" alt="">
                            <div class="photo_title">
                                <h4>Lens</h4>
                            </div>
                        </div>
                        <div class="single_photography">
                            <img src="<?php echo base_url('assets/img/photography/single-1.jpg')?>" alt="">
                            <div class="photo_title">
                                <h4>Photography</h4>
                            </div>
                        </div>
                        <div class="single_photography">
                            <img src="<?php echo base_url('assets/img/photography/single-2.jpg')?>" alt="">
                            <div class="photo_title">
                                <h4>Travel Shot</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- photography_slider_area_end -->