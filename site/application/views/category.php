<?php $this->load->view('header');?>
<?php $this->load->view('carrossel');?>
<?php $this->load->view('recente');?>

    <!-- photo_gallery_start -->
    <div class="photo_gallery">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title mb-33">
                        <h3>Photo Gallery</h3>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="photo_gallery_active owl-carousel">
                        <div class="single_photo_gallery gallery_bg_1">
                            <div class="photo_caption">
                                <h3>Travel Camping</h3>
                            </div>
                        </div>
                        <div class="single_photo_gallery gallery_bg_2">
                            <div class="photo_caption">
                                <h3>Travel Camping</h3>
                            </div>
                        </div>
                        <div class="single_photo_gallery gallery_bg_1">
                            <div class="photo_caption">
                                <h3>Travel Camping</h3>
                            </div>
                        </div>
                        <div class="single_photo_gallery gallery_bg_2">
                            <div class="photo_caption">
                                <h3>Travel Camping</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- photo_gallery_end -->

    <!-- subscribe_newsletter_start -->
    <div class="subscribe_newsletter">
        <div class="container">
            <div class="black_bg">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="newsletter_text">
                            <h3>
                                Subscribe newsletter
                            </h3>
                            <p>Get updates to our newsletter and new articles</p>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="newsform">
                            <form action="#">
                                <input type="email" placeholder="Enter Your Email">
                                <button type="submit">sign up</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
    <?php $this->load->view('redes');?>
    <?php $this->load->view('footer');?>

    