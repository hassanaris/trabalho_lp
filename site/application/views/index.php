    <?php $this->load->view('header');?>
    

    <!-- welcome_protomedia_start -->
    <div class="welcome_protomedia">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <h3>Welcome to Photomedia <br>
                        photography blog</h3>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="add_here">
                        <a href="#">
                            <img src="<?php echo base_url('assets/img/add.jpg')?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- welcome_protomedia_end -->

    <!-- photographi_area_start -->
    <div class="photographi_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="single_photography photography_bg_1">
                        <div class="info">
                            <div class="info_inner">
                                <h3><a href="#">Essential Photography: Back <br>
                                        Button Focus</a></h3>
                                <div class="date_catagory d-flex align-items-center justify-content-between">
                                    <span>12 jun 2019</span>
                                    <span class="catagory">lightroom</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="single_photography photography_bg_2">
                        <div class="info">
                            <div class="info_inner">
                                <h3><a href="#">Think Tank Photo Retrospective <br>
                                        Shoulder</a></h3>
                                <div class="date_catagory d-flex align-items-center justify-content-between">
                                    <span>12 jun 2019</span>
                                    <span class="catagory">lightroom</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- photographi_area_end -->

    <?php $this->load->view('carrossel');?>   

    <!-- photo_gallery_start -->
    <div class="photo_gallery">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title mb-33">
                        <h3>Photo Gallery</h3>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="photo_gallery_active owl-carousel">
                        <div class="single_photo_gallery gallery_bg_1">
                            <div class="photo_caption">
                                <h3>Travel Camping</h3>
                            </div>
                        </div>
                        <div class="single_photo_gallery gallery_bg_2">
                            <div class="photo_caption">
                                <h3>Travel Camping</h3>
                            </div>
                        </div>
                        <div class="single_photo_gallery gallery_bg_1">
                            <div class="photo_caption">
                                <h3>Travel Camping</h3>
                            </div>
                        </div>
                        <div class="single_photo_gallery gallery_bg_2">
                            <div class="photo_caption">
                                <h3>Travel Camping</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- photo_gallery_end -->

    <!-- subscribe_newsletter_start -->
    <div class="subscribe_newsletter">
        <div class="container">
            <div class="black_bg">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="newsletter_text">
                            <h3>
                                Subscribe newsletter
                            </h3>
                            <p>Get updates to our newsletter and new articles</p>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="newsform">
                            <form action="#">
                                <input type="email" placeholder="Enter Your Email">
                                <button type="submit">sign up</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
        <?php $this->load->view('redes');?>
        <?php $this->load->view('footer');?>