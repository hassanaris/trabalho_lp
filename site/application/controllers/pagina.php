<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagina extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper('url');
    }

	public function index(){
        $dados['titulo'] = "Photomedia";
		$this->load->view('index',$dados);
    }
    public function about(){ 
        $dados['titulo'] = "Photomedia";
		$this->load->view('about', $dados);
    }
    public function category(){
        $dados['titulo'] = "Photomedia";
		$this->load->view('category', $dados);
    }

    public function login(){
        $dados['titulo'] = "Photomedia";
		$this->load->view('login', $dados);
    }

    public function contact(){
        $this->load->helper('form');
        $this->load->library(array('form_validation','email'));
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('assunto', 'Assunto', 'trim|required');
        $this->form_validation->set_rules('mensagem', 'Mensagem', 'trim|required');

        if($this->form_validation->run() == FALSE):
            $dados['formerror'] = validation_errors(); 
        else:
            $dados_from = $this->input->post();
            $dados['formerror'] = 'Enviado com sucesso!!'; 
        endif;

        $dados['titulo'] = "Photomedia";
		$this->load->view('contact', $dados);
    }

}
