<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');   
        $this->load->model('option_model', 'option');     
    }

    public function index(){
        if($this->option->get_option('setup_executado') == 1){
            redirect('setup/alterar', 'refresh');
        }else{
            redirect('setup/instalar', 'refresh');
        } 
    }

    public function instalar(){
        if($this->option->get_option('setup_executado') == 1):
            redirect('setup/alterar', 'refresh');
        endif;

        $this->form_validation->set_rules('login', 'Nome', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('senha2', 'Repita a senha', 'trim|required|min_length[5]|matches[senha]');

        if($this->form_validation->run() == FALSE):
            if(validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $dados_form = $this->input->post();
            $this->option->update_option('user_login', $dados_form['login']);
            $this->option->update_option('user_email', $dados_form['email']);
            $this->option->update_option('user_pass', password_hash($dados_form['senha'], PASSWORD_DEFAULT));

            $inserido = $this->option->update_option('setup_executado', 1);
            
            if($inserido):
                set_msg('<p>Sistema instalado</p>');
                redirect('setup/login', 'refresh');
            endif;
        endif;

        $dados['titulo'] = "Photomedia";
        $this->load->view('painel/setup', $dados);
        
    }
}
?> 